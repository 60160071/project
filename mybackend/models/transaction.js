const mongoose = require('mongoose')
const Schema = mongoose.Schema
const transactionSchema = new Schema({
  user: {
    type: String,
    required: true,
  },
  description: {
    type: String,
    required: true,
    enum: ['ฝากเงิน', 'ถอนเงิน'],
  },
  money: {
    type: Number,
    required: true,
  },
  datetime: {
    type: Date,
    required: true,
    unique: true,
  },
  userReference: String 
})

const TransactionModel = mongoose.model('transaction', transactionSchema,'transaction')

module.exports = TransactionModel