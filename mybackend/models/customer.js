const mongoose = require('mongoose')
const Schema = mongoose.Schema
const customerSchema = new Schema({
  user: {
    type: String,
    required: true,
    minlength: 3
  },
  password: String,
  firstname: String,
  lastname: String
})

const CustomerModel = mongoose.model('customer', customerSchema,'customer')

module.exports = CustomerModel
