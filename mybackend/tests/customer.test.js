const dbHandler = require('./db-handler')
const Customer = require('../models/customer')

beforeAll(async () => {
  await dbHandler.connect()
})

afterEach(async () => {
  await dbHandler.clearDatabase()
})

afterAll(async () => {
  await dbHandler.closeDatabase()
})

const customerComplete = {
  user: 'tar',
  password: '123456',
  firstname: 'theerasak',
  lastname: 'karakate'
}

const customerErrorUserEmpty = {
  user: '',
  password: '123456',
  firstname: 'theerasak',
  lastname: 'karakate'
}

const customerErrorUser2Alphabets = {
  user: 'ta',
  password: '123456',
  firstname: 'theerasak',
  lastname: 'karakate'
}


describe('Customer', () => {
  it('สามารถเพิ่ม customer ได้', async () => {
    let error = null
    try {
      const customer = new Customer(customerComplete)
      await customer.save()  
    } catch (e) {
      error = e
    }
    expect(error).toBeNull()
  })
    it('ไม่สามารถเพิ่ม customer ได้ เพราะ user เป็นช่องว่าง', async () => {
      let error = null
      try {
        const customer = new Customer(customerErrorUserEmpty)
        await customer.save()  
      } catch (e) {
        error = e
      }
      expect(error).not.toBeNull()
    })
    it('ไม่สามารถเพิ่ม customer ได้ เพราะ user เป็น 2 ตัว', async () => {
      let error = null
      try {
        const customer = new Customer(customerErrorUser2Alphabets)
        await customer.save()  
      } catch (e) {
        error = e
      }
      expect(error).not.toBeNull()
    })
})


