const dbHandler = require('./db-handler')
const Transaction = require('../models/transaction')

beforeAll(async () => {
  await dbHandler.connect()
})

afterEach(async () => {
  await dbHandler.clearDatabase()
})

afterAll(async () => {
  await dbHandler.closeDatabase()
})

const transactionComplete1 = {
  user: 'tar',
  description: 'ฝากเงิน',
  money: '1000',
  datetime: '2020-04-06T10:05:08.955+00:00',
}

const transactionComplete2 = {
  user: 'tar',
  description: 'ถอนเงิน',
  money: '1000',
  datetime: '2020-04-06T10:05:08.955+00:00',
}

const transactionError1 = {
  user: '',
  description: 'ฝากเงิน',
  money: '1000',
  datetime: '2020-04-06T10:05:08.955+00:00',
}

const transactionError2 = {
  user: '123456',
  description: '',
  money: '1000',
  datetime: '2020-04-06T10:05:08.955+00:00',
}

const transactionError3 = {
  user: '123456',
  description: 'ฝากเงิน',
  money: '',
  datetime: '2020-06-06T10:05:08.955+00:00',
}

const transactionError4 = {
  user: '123456',
  description: 'ถอนเงิน',
  money: '1000',
  datetime: '',
}

const transactionError5 = {
  user: 'tar',
  description: 'ขโมยเงิน',
  money: '1000',
  datetime: '2020-04-06T10:05:08.955+00:00',
}

const transactionError6 = {
  user: 'tar',
  description: 'ฝากเงิน',
  money: 'AAAAAA',
  datetime: '2020-04-06T10:05:08.955+00:00',
}

const transactionError7 = {
  user: 'tar',
  description: 'ฝากเงิน',
  money: '1000',
  datetime: '2020-04-06T10:05:08.955+00:00',
}

describe('Transaction', () => {
  it('สามารถเพิ่ม Transaction ได้ ฝากเงิน', async () => {
    let error = null
    try {
      const transaction = new Transaction(transactionComplete1)
      await transaction.save()
    } catch (e) {
      error = e
    }
    expect(error).toBeNull()
  })

  it('สามารถเพิ่ม Transaction ได้ ถอนเงิน', async () => {
    let error = null
    try {
      const transaction = new Transaction(transactionComplete2)
      await transaction.save()
    } catch (e) {
      error = e
    }
    expect(error).toBeNull()
  })

  it('ไม่สามารถเพิ่ม Transaction ได้ เพราะ userเป็นช่องว่าง', async () => {
    let error = null
    try {
      const transaction = new Transaction(transactionError1)
      await transaction.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })

  it('ไม่สามารถเพิ่ม Transaction ได้ เพราะ description เป็นช่องว่าง', async () => {
    let error = null
    try {
      const transaction = new Transaction(transactionError2)
      await transaction.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })

  it('ไม่สามารถเพิ่ม Transaction ได้ เพราะ money เป็นช่องว่าง', async () => {
    let error = null
    try {
      const transaction = new Transaction(transactionError3)
      await transaction.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })

  it('ไม่สามารถเพิ่ม Transaction ได้ เพราะ datetime เป็นช่องว่าง', async () => {
    let error = null
    try {
      const transaction = new Transaction(transactionError4)
      await transaction.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })

  it('ไม่สามารถเพิ่ม Transaction ได้ เพราะ description เป็น ขโมยเงิน', async () => {
    let error = null
    try {
      const transaction = new Transaction(transactionError5)
      await transaction.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })

  it('ไม่สามารถเพิ่ม Transaction ได้ เพราะ money เป็น AAAAAA', async () => {
    let error = null
    try {
      const transaction = new Transaction(transactionError6)
      await transaction.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })

})
