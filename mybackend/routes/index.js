const express = require("express");
const router = express.Router();

module.exports = router;

router.use('/customer', require('./customer'))
router.use('/transaction', require('./transaction'))

