const express = require("express");
const router = express.Router();
const customerController = require('../controller/CustomerController'); 
module.exports = router;

router.get('/', customerController.getCustomer)
router.get('/:user', customerController.getCustomerByUser)
router.post('/login', customerController.getCustomerLogin)
router.post('/register', customerController.addCustomer)