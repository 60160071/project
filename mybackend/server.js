const express = require('express')
const mongoose = require('mongoose')
const cors = require('cors')
const app = express()
const uri = 'mongodb://localhost:27017/db';
app.use(express.json())

app.use(cors())
mongoose.connect(uri, {
  useNewUrlParser: true
})

app.use("/api", require("./routes"));


app.listen(9000, () => {
  console.log('Application is running on port 9000')
})